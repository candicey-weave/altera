package com.gitlab.candicey.altera

import com.gitlab.candicey.altera.alteration.TransformManager
import com.gitlab.candicey.altera.extension.createTempJar
import com.gitlab.candicey.zenithloader.ZenithLoader
import com.gitlab.candicey.zenithloader.dependency.BytesProvider
import com.gitlab.candicey.zenithloader.dependency.Dependency
import com.gitlab.candicey.zenithloader.dependency.DependencyUtilities
import com.gitlab.candicey.zenithloader.dependency.DependencyUtilities.fieldVersionChecker
import java.lang.instrument.Instrumentation

object Altera {
    fun alter(
        instrumentation: Instrumentation,
        groupId: String,
        name: String,
        bytesProvider: BytesProvider,
        transformManager: TransformManager? = null,
        version: String = transformManager?.version ?: error("Version must be provided"),
        namespace: String? = null,
        bootstrapClass: String? = null,
        versionClassPath: String = transformManager?.versionDeterminerClassName ?: "$groupId/$name/$version/Altera\$VersionIndicator"
    ): Dependency {
        info("Altering: $groupId:$name:$version")

        val dependency = Dependency(
            groupId = groupId,
            name = name,
            bytesProvider = createBytesProvider(bytesProvider, transformManager),
            version = version,
            namespace = namespace,
            checker = fieldVersionChecker(
                ownerClass = { versionClassPath.replace('/', '.') },
                fieldName = { "VERSION" },
                fieldValueChecker = { it == version }
            ),
            initiator = bootstrapClass?.let { { DependencyUtilities.VersionJson.Initiator(it) } }
        ).also(Dependency::tryAppendJarToClassLoader)

        ZenithLoader.loadDependencies(dependency, inst = instrumentation)

        info("Altered: $groupId:$name:$version")

        return dependency
    }

    private fun createBytesProvider(
        originalBytesProvider: BytesProvider,
        transformManager: TransformManager?,
    ): BytesProvider = {
        val originalJar = originalBytesProvider.createTempJar()

        if (transformManager != null) {
            val newJar = createTempFile("Altera-bytesprovider-transformed", ".jar")
            transformManager.transform(originalJar, newJar)
            newJar.readBytes()
        } else {
            originalJar.readBytes()
        }
    }
}