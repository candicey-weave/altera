package com.gitlab.candicey.altera

import com.gitlab.candicey.zenithcore.extension.sha256
import com.gitlab.candicey.zenithloader.ZenithLoader
import com.gitlab.candicey.zenithloader.dependency.BytesProvider
import com.gitlab.candicey.zenithloader.dependency.Dependency
import com.gitlab.candicey.zenithloader.dependency.Dependency.Companion.toCheckResult
import java.io.File
import java.net.URL

internal fun info(message: String) = LOGGER.info("[Altera] $message")

internal fun warn(message: String) = LOGGER.warn("[Altera] $message")

internal fun debug(message: String) = LOGGER.debug("[Altera] $message")

fun downloadFile(url: String, path: String, sha256: String? = null, async: Boolean = false, success: () -> Unit = {}, failure: () -> Unit = {}) {
    val file = File(path)
    if (file.exists()) {
        if (sha256 != null && sha256 != file.sha256) {
            warn("File $path has an invalid checksum, redownloading")
            file.delete()
        } else {
            success()
            return
        }
    }

    debug("Downloading $url to $path")

    val download = {
        try {
            val bytes = URL(url).readBytes()

            if (!file.exists()) {
                file.parentFile?.mkdirs()
                file.createNewFile()
            }

            file.writeBytes(bytes)

            if (sha256 != null && sha256 != file.sha256) {
                warn("File $path has an invalid checksum, redownloading")
                file.delete()
                downloadFile(url, path, sha256, async, success, failure)
            } else {
                success()
            }
        } catch (e: Exception) {
            warn("Failed to download $url to $path")
            failure()
        }
    }

    if (async) {
        Thread(download).start()
    } else {
        download()
    }
}

fun downloadTempFile(url: String, sha256: String? = null, async: Boolean = false, success: (File) -> Unit = {}, failure: () -> Unit = {}, deleteOnExit: Boolean = true) {
    val file = createTempFile(deleteOnExit = deleteOnExit)

    downloadFile(
        url = url,
        path = file.path,
        sha256 = sha256,
        async = async,
        success = { success(file) },
        failure = {
            file.delete()
            failure()
        }
    )
}

fun createTempFile(prefix: String? = null, suffix: String? = null, deleteOnExit: Boolean = true) = File
    .createTempFile(prefix ?: "Altera", suffix)
    .also { if (deleteOnExit) it.deleteOnExit() }

fun simpleBytesProvider(url: String, sha256: String? = null, groupId: String, name: String, version: String): BytesProvider = {
    val downloadBytesProvider = {
        var bytes: ByteArray? = null

        println("Downloading temporary $name jar...")
        downloadTempFile(
            url = url,
            sha256 = sha256,
            success = { bytes = it.readBytes() },
            failure = { error("Failed to download $name jar from $url") }
        )

        bytes ?: error("OneConfig bytes not found")
    }

    val dependency = Dependency(
        groupId = groupId,
        name = name,
        version = version,
        bytesProvider = { downloadBytesProvider() },
        checker = { false.toCheckResult() }
    )

    with(dependency.file) {
        if (!exists()) {
            ZenithLoader.loadDependencies(dependency, initiateDependencies = false, appendToClassLoader = false)
        }

        readBytes()
    }
}
