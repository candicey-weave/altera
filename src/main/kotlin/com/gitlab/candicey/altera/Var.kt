package com.gitlab.candicey.altera

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger

internal val LOGGER: Logger by lazy { LogManager.getLogger("Altera") }