package com.gitlab.candicey.altera.asm

import com.gitlab.candicey.altera.extension.toClassNode
import com.gitlab.candicey.altera.extension.toClassReader
import org.objectweb.asm.ClassReader
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.ClassNode
import java.net.URLClassLoader
import kotlin.io.path.Path

open class BytesClassWriter(
    flags: Int,
    val bytesProvider: (String) -> ByteArray?,
    classReader: ClassReader? = null,
) : ClassWriter(classReader, flags) {
    private fun ClassNode.isInterface() = (access and Opcodes.ACC_INTERFACE) != 0

    private fun ClassReader.isAssignableFrom(target: ClassReader): Boolean {
        val classes = ArrayDeque(listOf(target))

        while (classes.isNotEmpty()) {
            val classReader = classes.removeFirst()
            if (classReader.className == className) {
                return true
            }

            classes.addAll(
                (listOfNotNull(classReader.superName) + classReader.interfaces).map { ClassReader(bytesProvider(it)) }
            )
        }

        return false
    }

    override fun getCommonSuperClass(type1: String, type2: String): String {
        var class1 = bytesProvider(type1)?.toClassReader() ?: error("Failed to find type1 $type1")
        val class2 = bytesProvider(type2)?.toClassReader() ?: error("Failed to find type2 $type2")

        return when {
            class1.isAssignableFrom(class2) -> type1
            class2.isAssignableFrom(class1) -> type2
            class1.toClassNode().isInterface() || class2.toClassNode().isInterface() -> "java/lang/Object"
            else -> {
                while (!class1.isAssignableFrom(class2))
                    class1 = bytesProvider(class1.superName)!!.toClassReader()

                return class1.className
            }
        }
    }
}

open class ClassLoaderClassWriter(
    flags: Int,
    classLoader: ClassLoader = ClassLoader.getSystemClassLoader(),
    classReader: ClassReader? = null,
) : BytesClassWriter(flags, classLoader.asBytesProvider(), classReader) {
    companion object {
        private fun ClassLoader.asBytesProvider() = { name: String ->
            getResourceAsStream(name.replace('.', '/') + ".class")?.readBytes()
        }
    }
}

open class JarClassWriter(
    flags: Int,
    vararg jarFiles: String,
    classReader: ClassReader? = null,
) : ClassLoaderClassWriter(flags, createClassLoader(jarFiles), classReader) {
    companion object {
        private fun createClassLoader(jarFiles: Array<out String>) = URLClassLoader(jarFiles.map { Path(it).toUri().toURL() }.toTypedArray())
    }
}