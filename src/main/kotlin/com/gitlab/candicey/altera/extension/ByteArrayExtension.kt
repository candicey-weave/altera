package com.gitlab.candicey.altera.extension

import org.objectweb.asm.ClassReader
import org.objectweb.asm.tree.ClassNode

fun ByteArray.toClassReader(): ClassReader = ClassReader(this)

fun ByteArray.toClassNode(): ClassNode = toClassReader().toClassNode()