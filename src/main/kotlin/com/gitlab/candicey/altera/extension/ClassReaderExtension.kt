package com.gitlab.candicey.altera.extension

import org.objectweb.asm.ClassReader
import org.objectweb.asm.tree.ClassNode

fun ClassReader.toClassNode(): ClassNode = ClassNode().also { accept(it, 0) }