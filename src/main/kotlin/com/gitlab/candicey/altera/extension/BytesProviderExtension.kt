package com.gitlab.candicey.altera.extension

import com.gitlab.candicey.altera.createTempFile
import com.gitlab.candicey.zenithloader.dependency.BytesProvider
import java.io.File

fun BytesProvider.createTempJar(): File {
    val tempFile = createTempFile("Altera-bytesprovider-temp", ".jar")
    tempFile.writeBytes(this())
    return tempFile
}