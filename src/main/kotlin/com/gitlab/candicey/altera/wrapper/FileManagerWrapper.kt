package com.gitlab.candicey.altera.wrapper

import com.gitlab.candicey.zenithcore.util.reflectClass
import com.gitlab.candicey.zenithcore.util.reflectField
import com.gitlab.candicey.zenithcore.util.reflectMethod
import java.io.File

object FileManagerWrapper {
    val originalClass = reflectClass("net/weavemc/loader/util/FileManager")

    val instance = reflectField(originalClass, "INSTANCE")[null]!!

    val vanillaMinecraftJar by lazy { instance.reflectMethod("getVanillaMinecraftJar", returnType = File::class.java)(instance) as File }
}