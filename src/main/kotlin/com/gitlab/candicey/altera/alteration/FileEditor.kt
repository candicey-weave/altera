package com.gitlab.candicey.altera.alteration

abstract class FileEditor(vararg val fileNames: String) {
    abstract fun edit(name: String, bytes: ByteArray): ByteArray
}