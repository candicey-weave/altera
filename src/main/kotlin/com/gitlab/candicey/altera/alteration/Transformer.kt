package com.gitlab.candicey.altera.alteration

import org.objectweb.asm.tree.ClassNode

abstract class Transformer(vararg val classes: String) {
    abstract fun transform(classNode: ClassNode)
}