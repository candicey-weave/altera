package com.gitlab.candicey.altera.alteration

import com.gitlab.candicey.altera.asm.JarClassWriter
import com.gitlab.candicey.altera.debug
import com.gitlab.candicey.altera.extension.toClassNode
import com.gitlab.candicey.altera.extension.toClassReader
import com.gitlab.candicey.altera.info
import com.gitlab.candicey.altera.wrapper.FileManagerWrapper
import com.gitlab.candicey.zenithcore.util.weave.internalNameOf
import com.gitlab.candicey.zenithloader.remapJarToTemp
import net.weavemc.internals.asm
import org.objectweb.asm.ClassReader
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.Opcodes
import org.objectweb.asm.Opcodes.ACC_PUBLIC
import org.objectweb.asm.Opcodes.ACC_STATIC
import org.objectweb.asm.commons.ClassRemapper
import org.objectweb.asm.commons.Remapper
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.FieldNode
import org.objectweb.asm.tree.LabelNode
import org.objectweb.asm.tree.MethodNode
import java.io.File
import java.util.jar.JarFile
import java.util.jar.JarOutputStream
import java.util.zip.ZipEntry

typealias ClassTransformer = List<Pair<String, (ClassNode) -> Unit>>

@Suppress("MemberVisibilityCanBePrivate")
open class TransformManager(
    val transformers: MutableList<Transformer> = mutableListOf(),
    val remappers: MutableList<Remapper> = mutableListOf(),
    val classesToAdd: MutableList<Class<*>> = mutableListOf(),
    val filesToAdd: MutableList<String> = mutableListOf(),
    val filesToAddChangePath: MutableMap<String, String> = mutableMapOf(),
    val filesEditor: MutableList<FileEditor> = mutableListOf(),
    val finalBytesConsumer: (MutableMap<String, ByteArray>) -> Unit = {},
    val classPath: List<File> = listOf(FileManagerWrapper.vanillaMinecraftJar),
    val versionDeterminerClassName: String? = null,
    val version: String? = null
) {
    val mappedClassPath = classPath.map { it.remapJarToTemp("official").canonicalPath }.toTypedArray()

    open fun transform(jarIn: File, jarOut: File) {
        val transformers = getTransformers()

        info("Transforming jar ${jarIn.canonicalPath} to ${jarOut.canonicalPath}")

        val bytesCache = mutableMapOf<String, ByteArray>()

        JarFile(jarIn).use { jar ->
            val mappedTransformers = transformers.map {
                (if (it.first.isEmpty()) "" else it.first.replace(
                    '.',
                    '/'
                ) + ".class") to it.second
            }
            val isAll = mappedTransformers.any { it.first.isEmpty() }

            for (entry in jar.entries().asSequence()) {
                val name = entry.name

                if (name.endsWith(".class")) {
                    var bytes = jar.getInputStream(entry).readBytes()
                    var classReader: ClassReader?

                    for (remapper in remappers) {
                        classReader = bytes.toClassReader()
                        val classWriter = createClassWriter(classReader, jarIn)

                        classReader.accept(ClassRemapper(classWriter, remapper), 0)

                        bytes = classWriter.toByteArray()
                    }

                    val classNode = bytes.toClassNode()

                    if (isAll || mappedTransformers.any { name == it.first }) {
                        mappedTransformers
                            .filter { if (it.first.isEmpty()) true else name == it.first }
                            .forEach {
                                it.second(classNode)
                            }
                    }

                    val classWriter = createClassWriter(null, jarIn)
                    classNode.accept(classWriter)

                    bytesCache[classNode.name + ".class"] = classWriter.toByteArray()
                } else {
                    bytesCache[name] = jar.getInputStream(entry).readBytes()
                }
            }
        }

        addFilesFinal(bytesCache)
        injectVersionDeterminer(bytesCache)

        jarOut.parentFile?.mkdirs()
        jarOut.createNewFile()

        JarOutputStream(jarOut.outputStream()).use { jarOutputStream ->
            for ((name, bytes) in bytesCache) {
                jarOutputStream.putNextEntry(ZipEntry(name))
                jarOutputStream.write(bytes)
                jarOutputStream.closeEntry()
            }
        }

        debug("Transformed jar written to ${jarOut.canonicalPath}")
    }

    @JvmName("privateGetTransformers")
    private fun getTransformers(): ClassTransformer {
        val classTransformers = mutableListOf<Pair<String, (ClassNode) -> Unit>>()

        for (transformer in transformers) {
            debug("transformer = $transformer")

            val classes = transformer.classes
            val classTransformer = transformer::transform

            if (classes.isEmpty()) {
                classTransformers += "" to classTransformer
            } else {
                classTransformers += classes.map { it to classTransformer }
            }
        }

        return classTransformers
    }

    private fun addFilesFinal(finalBytesMap: MutableMap<String, ByteArray>) {
        for (clazz in classesToAdd) {
            val internalName = clazz.name.replace('.', '/') + ".class"
            val classReader = ClassReader(clazz.getResourceAsStream("/$internalName"))
            val classNode = ClassNode()
            classReader.accept(classNode, 0)

            val classWriter = ClassWriter(ClassWriter.COMPUTE_MAXS)
            classNode.accept(classWriter)

            finalBytesMap[internalName] = classWriter.toByteArray()

            debug("finalBytesMap[$internalName] = ${finalBytesMap[internalName]}")
        }

        for (file in filesToAdd) {
            finalBytesMap[file] = javaClass.getResourceAsStream("/$file").readBytes()

            debug("finalBytesMap[$file] = ${finalBytesMap[file]}")
        }

        for ((file, newPath) in filesToAddChangePath) {
            finalBytesMap[newPath] = javaClass.getResourceAsStream("/$file").readBytes()

            debug("finalBytesMap[$newPath] = ${finalBytesMap[newPath]}")
        }

        for (fileEditor in filesEditor) {
            val fileNames = fileEditor.fileNames

            for ((name, bytes) in finalBytesMap) {
                if (fileNames.isEmpty() || fileNames.any { it == name }) {
                    finalBytesMap[name] = fileEditor.edit(name, bytes)
                }
            }
        }

        finalBytesConsumer(finalBytesMap)
    }

    open fun injectVersionDeterminer(bytesMap: MutableMap<String, ByteArray>) {
        val dependencyVersion = version ?: return
        val className = (versionDeterminerClassName ?: return).replace('.', '/')
        val resourceName = "$className.class"

        val classNode = ClassNode().apply {
            name = className

            superName = internalNameOf<Any>()

            version = Opcodes.V1_8

            fields = mutableListOf(
                FieldNode(
                    ACC_PUBLIC or ACC_STATIC,
                    "VERSION",
                    "Ljava/lang/String;",
                    null,
                    null
                )
            )

            methods = mutableListOf(
                MethodNode(ACC_STATIC, "<clinit>", "()V", null, null).apply {
                    instructions = asm {
                        ldc(dependencyVersion)
                        putstatic(className, "VERSION", "Ljava/lang/String;")
                        _return
                    }
                },

                MethodNode(ACC_PUBLIC, "<init>", "()V", null, null).apply {
                    instructions = asm {
                        aload(0)
                        invokespecial(internalNameOf<Any>(), "<init>", "()V")
                        _return
                    }
                },

                MethodNode(ACC_PUBLIC, "hashCode", "()I", null, null).apply {
                    instructions = asm {
                        ldc(0)
                        ireturn
                    }
                },

                MethodNode(ACC_PUBLIC, "equals", "(Ljava/lang/Object;)Z", null, null).apply {
                    instructions = asm {
                        aload(0)
                        aload(1)
                        val label = LabelNode()
                        if_acmpne(label)
                        iconst_1
                        ireturn
                        +label
                        iconst_0
                        ireturn
                    }
                }
            )
        }

        val classWriter = ClassWriter(ClassWriter.COMPUTE_MAXS)
        classNode.accept(classWriter)

        bytesMap[resourceName] = classWriter.toByteArray()

        info("Injected version determiner class $className with version $dependencyVersion")
    }

    private fun createClassWriter(classReader: ClassReader?, jarIn: File): ClassWriter =
        try {
            JarClassWriter(
                flags = ClassWriter.COMPUTE_FRAMES,
                jarIn.canonicalPath,
                *mappedClassPath,
            )
        } catch (e: Exception) {
            info("Failed to compute frames for ${classReader?.className}")
            e.printStackTrace()

            JarClassWriter(
                flags = ClassWriter.COMPUTE_MAXS,
                jarIn.canonicalPath,
                *mappedClassPath,
            )
        }
}