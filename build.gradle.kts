plugins {
    kotlin("jvm") version "1.9.23"
    `maven-publish`
    id("com.gitlab.candicey.stellar") version "0.2.1"
}

val projectName: String by project
val projectGroup: String by project
val projectVersion: String by project
val gitlabProjectId: String by project

group = projectGroup
version = projectVersion

stellar {
    relocate {
    }

    dependencies {
        zenithCore version "2.0.1"
        zenithLoader version "0.3.5"
    }
}

repositories {
    mavenCentral()
    mavenLocal()
    maven("https://repo.weavemc.dev/releases")
}

dependencies {
    implementation(libs.asm)
    implementation(libs.asmCommons)
    implementation(libs.kxSerJSON)
    implementation(libs.log4jCore)

    implementation("net.weavemc:loader:1.0.0-PRE2")
    implementation("net.weavemc:internals:1.0.0-PRE2")
    implementation("io.github.770grappenmaker:mappings-util:0.1.4")

    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnitPlatform()
}

java {
    withSourcesJar()
    withJavadocJar()
}

kotlin {
    jvmToolchain(8)
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            from(components["java"])
        }
    }

    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/$gitlabProjectId/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = findProperty("gitLabPrivateToken") as String?
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }
}